public class ViewSuppliers { 
    public ViewSuppliers(ApexPages.StandardSetController controller) {
        
        //queryResult = controller.getRecords(); //Database.query('SELECT Id, name, Supplier__c, Address__c, Country__c, State__c, Contact_Phone__c FROM Supplier__c') ;
    }



    public String test { get; set; }

    public PageReference quicksave() {
        ApexPages.StandardController stdcontroller;
        //Id sid = stdcontroller.getID();
        //test = Component.Apex.page.controller.value;
        
        //test = 'andre';
        //test = stdcontroller.getID();
        System.Debug(test);
        return null;
    }
    
   // public ViewSuppliers(){
        
   // }



    



    public Supplier__c [] myObj;
    public String searchValue {  get; set; }
    public String getSelectedObject() { return this.selectedObject; }
    public void setSelectedObject(String s) { this.selectedObject= s; }
    public String selectedObject;    
    public List<Supplier__c> queryResult { get; set; }
// { get {return queryResult; } set { queryResult = value ; }}

    

    public List<SelectOption> getObjectOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('- None -','- None -'));
        options.add(new SelectOption('Country','Country'));
        options.add(new SelectOption('State','State'));
        return options;
    }


    
    public List<SelectOption> getRecordTypeOptions() {
        List<SelectOption> options = new List<SelectOption>();
        Set<String> uniqueCustomObjectSet = new Set<String>();
        
        options.add(new SelectOption('All','All'));

        for(Supplier__c sl:[Select Country__c, State__c From Supplier__c])
        {
          if (selectedObject == 'Country') {
            if (sl.Country__c == null){
                uniqueCustomObjectSet.add('None Specified');
            }
            
            else {
                uniqueCustomObjectSet.add(sl.Country__c);
            }
          }
          if (selectedObject == 'State') {
            if (sl.State__c == null){
                uniqueCustomObjectSet.add('None Specified');
            }
            
            else {
                uniqueCustomObjectSet.add(sl.State__c);
            }
          }
          if (selectedObject == '- None -'){
             // uniqueCustomObjectSet.add('None Specified');
          }
        }
        List<String> uniqueCustomObjectList = new List<String>();
        uniqueCustomObjectList.addAll(uniqueCustomObjectSet);
        for(integer i=0; i<uniqueCustomObjectList.size(); i++){
                        if (uniqueCustomObjectList[i] == null){}
            else
            {
            options.add(new SelectOption(uniqueCustomObjectList[i],uniqueCustomObjectList[i]));
            }
        }
        

        return options;
 
    }


    public String RecordTypeID { get; set; }
    
    
    public PageReference query() {
       
        // create the query string
        String qryString;
        If((RecordTypeID == '')||(RecordTypeID == null)||(RecordTypeID == 'All')||(selectedObject == '')||(selectedObject == null)||(selectedObject == '- None -')){
            qryString = 'SELECT Id, name, Supplier__c, Address__c, Country__c, State__c, Contact_Phone__c FROM Supplier__c'; 
        }
        else If ((RecordTypeID == 'None Specified') && (selectedObject == 'Country')){
            qryString = 'SELECT Id, name, Supplier__c, Address__c, Country__c, State__c, Contact_Phone__c FROM Supplier__c where Country__c = \'\'' ; 

        }
        else If ((RecordTypeID == 'None Specified') && (selectedObject == 'State')){
            qryString = 'SELECT Id, name, Supplier__c, Address__c, Country__c, State__c, Contact_Phone__c FROM Supplier__c where State__c = \'\'' ; 

        }
        else If (selectedObject == 'State'){
            qryString = 'SELECT Id, name, Supplier__c, Address__c, Country__c, State__c, Contact_Phone__c FROM Supplier__c where State__c = \'' + RecordTypeID  + '\'' ; 

        }
        else If (selectedObject == 'Country'){
            qryString = 'SELECT Id, name, Supplier__c, Address__c, Country__c, State__c, Contact_Phone__c FROM Supplier__c where Country__c = \'' + RecordTypeID  + '\'' ; 

        }
        else {
            
            qryString = 'SELECT Id, name, Supplier__c, Address__c, Country__c, State__c, Contact_Phone__c FROM Supplier__c' ; 
        }
        // execute the query
        queryResult = Database.query(qryString) ;       
        return null;
    } 
    
    public PageReference searchquery() {
        if ((searchValue == null) || (searchValue == '') || searchValue.length() < 2) {
        searchValue = 'Please enter Text';
        }    
        // Supplier__c [] myQuery  = [Select Id, name, Supplier__c, Address__c, Country__c, State__c from Supplier__c];
        //Supplier__c myQuery = new Supplier__c();
        //Supplier__c [] queryResult  = (List<Supplier__c>)[FIND :searchValue RETURNING Supplier__c(State__c, name, Supplier__c, Address__c, Country__c)][0];
        
        String texSearch = '*' + searchValue + '*';
        queryResult  = (List<Supplier__c>)[FIND :texSearch RETURNING Supplier__c(State__c, name, Supplier__c, Address__c, Country__c, Contact_Phone__c)][0];
        //        search.query('FIND\'' + searchValue + '*\'IN ALL FIELDS RETURNING Supplier__c');
        //queryResult = this.queryResult;
        //queryResult.addAll(myQuery);
        return null;
    }

}