public class CrewList{ 
    public CrewList(ApexPages.StandardSetController controller) {
  queryResult = Database.query('SELECT Id, name, Code__c, Last_Name__c, First_Name__c, Rank__c, Nationality__c, Vessel__c FROM Crew_Member__c') ;

           BubbleSort(queryResult);
    }


    public String test { get; set; }
    public String RecordTypeID { get; set; }


    public List<SelectOption> getObjectOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('All','All'));
        options.add(new SelectOption('Name','Code'));
        options.add(new SelectOption('Last_Name__c','Last Name'));
        options.add(new SelectOption('First_Name__c','First Name'));
        options.add(new SelectOption('Rank__c','Rank'));
        options.add(new SelectOption('Nationality__c','Nationality'));
        options.add(new SelectOption('Vessel__c','Vessel'));
        return options;
    }


    
        public PageReference query() {
        String qryString;
        if(selectedObject == 'All'){
         qryString = 'SELECT Id, name, Code__c, Last_Name__c, First_Name__c, Rank__c, Nationality__c, Vessel__c FROM Crew_Member__c';
        }else {
          qryString = 'SELECT Id, name, Code__c, Last_Name__c, First_Name__c, Rank__c, Nationality__c, Vessel__c FROM Crew_Member__c where ' + selectedObject + '= \'' + searchValue + '\'';
        }
        queryResult = Database.query(qryString) ;
        BubbleSort(queryResult);
         //sortList(queryResult,'Code__c','asc'); 
        //queryResult = sortFieldValue(queryResult);
        return null;
        } 

    public String searchValue {  get; set; }
    public String getSelectedObject() { return this.selectedObject; }
    public void setSelectedObject(String s) { this.selectedObject= s; }
    public String selectedObject;    
    public List<Crew_Member__c> queryResult { get; set; }

    public void BubbleSort(List<Crew_Member__c> inArray) {
    Boolean swapped = false;
   
    do {
        swapped = false;
        for (Integer i = 0; i < (inArray.size() - 1); i++){
            Integer val1 = Integer.valueOf(inArray[i].Code__c);
            Integer val2 = Integer.valueOf(inArray[i + 1].Code__c);
            if (val1 > val2) {
                swap(inArray, i, i+1);
                swapped = true;
            } // if
        } // for
       
    } while (swapped == true);
} // method
private void swap(List<Crew_Member__c> inArray, Integer idx, Integer jdx) {
    String tempStr;
   
    tempStr = inArray[idx].Code__c;
    inArray[idx].Code__c = inArray[jdx].Code__c;
    inArray[jdx].Code__c = tempStr;
}
}