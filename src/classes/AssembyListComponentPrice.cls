public class AssembyListComponentPrice {

        
        public List<Supplier__c> componentList;  //   list of components to appear in the multi-line
        
        public PageReference reset() {
            componentList = [select name, Supplier__c, Address__c, Country__c
                               from Supplier__c
                               where Supplier__c = :System.currentPageReference().getParameters().get('id')
                               order by name];
            return null;
        }  
        
        
        public List<Supplier__c> getComponents() {
            if(componentList == null) reset(); 
            return componentList;
        }
        public void setComponents(List<Supplier__c> Components) {
            componentList = Components;
        }
        public PageReference save() {
            upsert componentList;
            return null;
            
            
        }
        public PageReference toAssembly() {
            PageReference pageRef = new PageReference('/' + ApexPages.currentPage().getParameters().get('id'));
            pageRef.setRedirect(true);
            
            return pageRef;
        }

        public PageReference add() {
            componentList.add(New Supplier__c()
            
            );
            return null;
                
        }

        public Supplier__c getAssembly() {
            return [select name, Supplier__c from Supplier__c 
                     where id = :ApexPages.currentPage().getParameters().get('id')]; 
           
           }
        
        
                        
              
               
}