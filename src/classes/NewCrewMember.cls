public class NewCrewMember{

    public String message { get; set; }

    public PageReference cancel() {

        return new PageReference('/apex/CrewList');
    }


    

    
    public Crew_Member__c member {get; private set;}

    public NewCrewMember() {
        Id id = ApexPages.currentPage().getParameters().get('id');
        member = (id == null) ? new Crew_Member__c() : 
        [SELECT name, Code__c, First_Name__c, Last_Name__c, Birth_Date__c, Birth_Place__c, Passport_No__c, Passport_expiry__c, Rank__c, Nationality__c, Vessel__c FROM Crew_Member__c WHERE id = :id];
    }

    public PageReference save() {
       try {
           upsert(member);
       } catch(System.DMLException e) {
           ApexPages.addMessages(e);
           message = e.getMessage();
          return null;
       }
       //  After Save, navigate to the default view page:  
    // return (new ApexPages.StandardController(supplier)).view();

       return new PageReference('/apex/CrewList');
   }

}