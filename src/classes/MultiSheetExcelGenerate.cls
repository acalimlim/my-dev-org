/**
author        : www.aslambari.com
date          : 26 May, 2011
description   : Main page which collects data for different sheets
*/
public class MultiSheetExcelGenerate{

    public DataModel dm {get;set;}
    public MultiSheetExcelGenerate(){
        dm = new DataModel();
        dm.data = new list<DataModel.Model>();
        /**
        Fetching Accout data
        */
        DataModel.Model model = new DataModel.Model();
        model.sheetname = 'Crew Members';
        model.headers = new list<string>{'Code','Last Name','First Name', 'Rank', 'Nationality', 'Vessel'};
        model.data = new list<map<string,string>>();
        for(Crew_Member__c act: [SELECT Code__c, Last_Name__c, First_Name__c, Rank__c, Nationality__c, Vessel__c FROM Crew_Member__c order by Last_Name__c] ){
            map<string,string> rowdata = new map<string,string>();
            //rowdata.put('Id',act.Id);
            rowdata.put('Code',act.Code__c);
            rowdata.put('Last Name',act.Last_Name__c);
            rowdata.put('First Name',act.First_Name__c);
            rowdata.put('Rank',act.Rank__c);
            rowdata.put('Nationality',act.Nationality__c);
            if (act.Vessel__c == null || act.Vessel__c == ''){
                rowdata.put('Vessel',' ');
            }
            else {
             rowdata.put('Vessel',act.Vessel__c);
            }
            model.data.add(rowdata);
        }
        dm.data.add(model);
        /*
        model = new DataModel.Model();
        model.sheetname = 'Contact';
        model.headers = new list<string>{'Id','Name','Phone','Email'};
        model.data = new list<map<string,string>>();
        for(Contact cnt: [SELECT Id, Name, Phone, Email from Contact Where Email != null and Phone != null]){
            map<string,string> rowdata = new map<string,string>();
            rowdata.put('Id',cnt.Id);
            rowdata.put('Name',cnt.Name);
            rowdata.put('Phone',cnt.Phone);
            rowdata.put('Email',cnt.Email);
            model.data.add(rowdata);
        }
        dm.data.add(model);*/
    }

}