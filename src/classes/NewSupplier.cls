public class NewSupplier {

    public PageReference cancel() {

        return new PageReference('/apex/ViewSuppliers');
    }

    public List<Supplier_Item__c> getMerchandise() {

        return (List<Supplier_Item__c>) setCon.getRecords();
    }
    
    public ApexPages.StandardSetController setCon {
        get {
        if(setCon == null) {
            String id = ApexPages.currentPage().getParameters().get('id');
            setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
            [Select id, Name, Supplier__c from Supplier_Item__c where Supplier__c in (Select id from Supplier__c where id =:id)]));
            
        } 
        return setCon;
        }
        set;
    }
    
    public Supplier__c supplier {get; private set;}

    public NewSupplier() {
        Id id = ApexPages.currentPage().getParameters().get('id');
        supplier = (id == null) ? new Supplier__c() : 
        [SELECT name, Supplier__c, Address__c, Country__c, City__c, State__c, Zip_Code__c, Contact_Name__c, Contact_Phone__c, Fax__c,
        Email_Address__c, Website__c FROM Supplier__c WHERE id = :id];
    }

    public PageReference save() {
       try {
           upsert(supplier);
       } catch(System.DMLException e) {
           ApexPages.addMessages(e);
          return null;
       }
       //  After Save, navigate to the default view page:  
    // return (new ApexPages.StandardController(supplier)).view();

       return new PageReference('/apex/ViewSuppliers');
   }

}