/* ============================================================
 * Part of this code has been modified from source that is part of the "apex-lang" open source project avaiable at:
 * 
 *      http://code.google.com/p/apex-lang/
 *
 * This code is licensed under the Apache License, Version 2.0.  You may obtain a 
 * copy of the License at:
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * ============================================================
 */


// A Generic View Object which is used to wrap generic SObject Field Data for Visualforce Page display.
public with sharing class GenericFieldVO 
{
    public String fieldLabel {get;set;}
    public String stringVal {get;set;}
    public Boolean boolVal {get;set;}
    public Date dateVal {get;set;}
    public Integer intVal {get;set;}
    public Double doubleVal {get;set;}
    public DateTime dateTimeVal {get;set;}
    public ID idVal {get;set;}
    public Boolean isBool {get;set;}
    
    private static final List<Schema.DisplayType> STRING_TYPES      = new List<Schema.DisplayType>{
    Schema.DisplayType.base64
    ,Schema.DisplayType.Email
    ,Schema.DisplayType.MultiPicklist
    ,Schema.DisplayType.Phone
    ,Schema.DisplayType.Picklist
    ,Schema.DisplayType.String
    ,Schema.DisplayType.TextArea
    ,Schema.DisplayType.URL
    };
    private static final List<Schema.DisplayType> INTEGER_TYPES     = new List<Schema.DisplayType>{
        Schema.DisplayType.Integer
    };
    private static final List<Schema.DisplayType> ID_TYPES          = new List<Schema.DisplayType>{
        Schema.DisplayType.ID
        ,Schema.DisplayType.Reference
    };
    private static final List<Schema.DisplayType> DOUBLE_TYPES      = new List<Schema.DisplayType>{
        Schema.DisplayType.Currency
        ,Schema.DisplayType.Double
        ,Schema.DisplayType.Percent
    };
    private static final List<Schema.DisplayType> DATETIME_TYPES    = new List<Schema.DisplayType>{
        Schema.DisplayType.DateTime
    };
    private static final List<Schema.DisplayType> DATE_TYPES        = new List<Schema.DisplayType>{
        Schema.DisplayType.Date
    };
    private static final List<Schema.DisplayType> BOOLEAN_TYPES     = new List<Schema.DisplayType>{
        Schema.DisplayType.Boolean
        ,Schema.DisplayType.Combobox
    };

    
    
    public GenericFieldVO(Schema.DescribeFieldResult sourceField, SObject source)
    {
        fieldLabel = sourceField.getLabel(); 
        if(contains(STRING_TYPES,sourceField.getType())){
            stringVal = (String)source.get(sourceField.getName());
        } else if(contains(INTEGER_TYPES,sourceField.getType())){
            intVal = (Integer)source.get(sourceField.getName());
        } else if(contains(ID_TYPES,sourceField.getType())){
            idVal = (ID)source.get(sourceField.getName());
        } else if(contains(DOUBLE_TYPES,sourceField.getType())){
           doubleVal = (Double)source.get(sourceField.getName());
        } else if(contains(DATETIME_TYPES,sourceField.getType())){
           dateTimeVal = (DateTime)source.get(sourceField.getName());
        } else if(contains(DATE_TYPES,sourceField.getType())){
            dateVal = (Date)source.get(sourceField.getName());
        } else if(contains(BOOLEAN_TYPES,sourceField.getType())){
           boolVal = (Boolean)source.get(sourceField.getName());
           isBool = true;
        }
    }
    
    private static Boolean contains(List<Schema.DisplayType> aListActingAsSet, Schema.DisplayType typeToCheck){
        if(aListActingAsSet != null && aListActingAsSet.size() > 0){
            for(Schema.DisplayType aType : aListActingAsSet){
                if(aType == typeToCheck){
                    return true;
                }
            }
        }
        return false;
    }
}