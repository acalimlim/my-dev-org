public class opportunityList2Con {
// ApexPages.StandardSetController must be instantiated
// for standard list controllers
public ApexPages.StandardSetController setCon {
get {
if(setCon == null) {
setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
[select id, name, Price__c from Merchandise__c]));
} 
return setCon;
}
set;
}
// Initialize setCon and return a list of records
public List<Merchandise__c> getOpportunities() {
return (List<Merchandise__c>) setCon.getRecords();
}
}