trigger createCasetest on Account (after update) {
    List<Case> caseCheck = new List<Case>();
    Set<Id> acctIdList = new Set<Id>();
    List<Case> caseList = new List<Case>();
    List<Account> acctList = new List<Account>();
    Integer recCount;
    
    for(integer x=0; x<1; x++) {
        
            acctIdList.add(trigger.new[x].Id);
        
    }
    caseCheck = [Select Subject from Case Where Accountid in: acctIdList];
    recCount = [Select count() from Case Where Accountid in: acctIdList];
    acctList = [Select Id, Name From Account Where Id in: acctIdList];
    
     Integer maxNumber = 0;
    
    if (recCount == 0){
        recCount = 1;
    }
    else {
    
    for(Case b : caseCheck){
        List<String> parts = b.Subject.Split(' ');
        Integer d = Integer.valueOf(parts[1]);
        if (maxNumber < d){
            maxNumber = d;
        }
    }
    recCount = maxNumber + 1;
    }
    for(Account a : acctList){
        
        Case newCase = new Case(
        Accountid = a.id,
        Subject = 'Subject ' + recCount
        );
        caseList.add(newCase);
        }
    insert caseList;
}